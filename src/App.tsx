import { useCallback, useEffect, useRef, useState } from "react";

import logoImg from "./assets/logo.png";
import DeleteConfirmation from "./components/DeleteConfirmation";
import Modal from "./components/Modal";
import Places from "./components/Places";
import {
  addPlaceToStore,
  getStoredPlaces,
  removePlaceFromStore,
} from "./services/local-storage.service";
import { sortPlacesByDistance } from "./services/location.service";
import { AVAILABLE_PLACES } from "./types/consts/data.const";
import { IPlace } from "./types/interfaces/place.interface";

const storedPlaces = getStoredPlaces();

function App() {
  const selectedPlace = useRef("");
  const [availablePlaces, setAvailablePlaces] = useState<IPlace[] | []>([]);
  const [pickedPlaces, setPickedPlaces] = useState<IPlace[] | []>(storedPlaces);
  const [modalIsOpen, setModalIsOpen] = useState<boolean>(false);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      const sortedPlaces = sortPlacesByDistance(
        AVAILABLE_PLACES,
        position.coords.latitude,
        position.coords.longitude
      );

      setAvailablePlaces(sortedPlaces);
    });
  }, []);

  function handleStartRemovePlace(placeId: string) {
    setModalIsOpen(true);
    selectedPlace.current = placeId;
  }

  function handleStopRemovePlace() {
    setModalIsOpen(false);
  }

  function handleSelectPlace(placeId: string) {
    setPickedPlaces((prevPickedPlaces) => {
      if (prevPickedPlaces.some((place: IPlace) => place.id === placeId)) {
        return prevPickedPlaces;
      }

      const place = AVAILABLE_PLACES.find((place) => place.id === placeId);
      if (place !== undefined) {
        return [place, ...prevPickedPlaces];
      }

      return prevPickedPlaces;
    });

    addPlaceToStore(placeId);
  }

  const handleRemovePlaceCallback = useCallback(function handleRemovePlace() {
    setPickedPlaces((prevPickedPlaces) =>
      prevPickedPlaces.filter(
        (place: IPlace) => place.id !== selectedPlace.current
      )
    );
    setModalIsOpen(false);
    removePlaceFromStore(selectedPlace.current);
  }, []);

  return (
    <>
      <Modal modalIsOpen={modalIsOpen}>
        <DeleteConfirmation
          onCancel={handleStopRemovePlace}
          onConfirm={handleRemovePlaceCallback}
        />
      </Modal>

      <header>
        <img src={logoImg} alt="Stylized globe" />
        <h1>PlacePicker</h1>
        <p>
          Create your personal collection of places you would like to visit or
          you have visited.
        </p>
      </header>
      <main>
        <Places
          title="I'd like to visit ..."
          fallbackText={"Select the places you would like to visit below."}
          places={pickedPlaces}
          onSelectPlace={handleStartRemovePlace}
        />
        <Places
          title="Available Places"
          places={availablePlaces}
          fallbackText="No places available yet"
          onSelectPlace={handleSelectPlace}
        />
      </main>
    </>
  );
}

export default App;
