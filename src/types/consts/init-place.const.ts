import forestWaterfallImg from "../../assets/forest-waterfall.jpg";

export const INIT_PLACE = {
  id: "p1",
  title: "Forest Waterfall",
  image: {
    src: forestWaterfallImg,
    alt: "A tranquil forest with a cascading waterfall amidst greenery.",
  },
  lat: 44.5588,
  lon: -80.344,
};
