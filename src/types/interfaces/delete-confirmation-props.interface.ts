import { MouseEventHandler } from "react";

export interface IDeleteConfirmation {
  onConfirm: () => void;
  onCancel: MouseEventHandler<HTMLButtonElement>;
}
