import { useEffect, useState } from "react";
import { TIMER } from "../types/consts/timeout-timer.const";

function ProgressBar(): JSX.Element {
  const [remainingTime, setRemainingTime] = useState(TIMER);

  useEffect(() => {
    const interval = setInterval(() => {
      setRemainingTime((prevTime) => prevTime - 10);
    }, 10);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return <progress value={remainingTime} max={TIMER}></progress>;
}

export default ProgressBar;
