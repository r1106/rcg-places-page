import { PropsWithChildren, useRef } from "react";
import { createPortal } from "react-dom";
import { IModalProps } from "../types/interfaces/modal-props.interface";

function Modal({
  modalIsOpen,
  children,
}: PropsWithChildren<IModalProps>): JSX.Element {
  const dialog = useRef<HTMLDialogElement>(null);

  return createPortal(
    <dialog className="modal" ref={dialog} open={modalIsOpen}>
      {modalIsOpen ? children : null}
    </dialog>,
    document.getElementById("modal") as HTMLElement
  );
}

export default Modal;
