import { AVAILABLE_PLACES } from "../types/consts/data.const";
import { IPlace } from "../types/interfaces/place.interface";

const STORENAME = "selectedPlaces";

function getStoredIds(): string[] | [] {
  let storedIds: string[] | [] = [];
  const storedSelectedPlaces = localStorage.getItem(STORENAME);

  if (storedSelectedPlaces) {
    storedIds = JSON.parse(storedSelectedPlaces);
  }

  return storedIds;
}

export function getStoredPlaces(): IPlace[] {
  const storedIds = getStoredIds();
  let storedPlaces: IPlace[] = [];

  if (storedIds) {
    storedPlaces = storedIds.map((placeId) => {
      const foundPlaces = AVAILABLE_PLACES.find(
        (place) => place.id === placeId
      );
      if (foundPlaces === undefined) {
        return [] as unknown as IPlace;
      }
      return foundPlaces;
    });
  }

  return storedPlaces;
}

export function addPlaceToStore(placeId: string): void {
  const storedIds = getStoredIds();
  if (storedIds && storedIds.indexOf(placeId as never) === -1) {
    localStorage.setItem(STORENAME, JSON.stringify([placeId, ...storedIds]));
  }
}

export function removePlaceFromStore(currentId: string): void {
  const storedIds = getStoredIds();
  if (storedIds) {
    localStorage.setItem(
      STORENAME,
      JSON.stringify(storedIds.filter((placeId) => placeId === currentId))
    );
  }
}
